

CREATE TABLE users (
user_id SERIAL PRIMARY KEY,
userName varchar(255) UNIQUE NOT NULL,
email varchar(255) NOT NULL,
password varchar(255) NOT NULL,
salt varchar(255)
);
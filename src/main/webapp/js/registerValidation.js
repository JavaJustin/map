$(document).ready(function() {

	$.validator.addMethod("complexPassword", function(value) {

		var rulesPassed = 0;

		if (/[A-Z]/.test(value)) {
			rulesPassed++;
		}
		if (/[a-z]/.test(value)) {
			rulesPassed++;
		}
		if (/[0-9]/.test(value)) {
			rulesPassed++;
		}
		if (/[^a-zA-Z0-9]/.test(value)) {
			rulesPassed++;
		}

		return rulesPassed >= 3;

	}, "Password is not complex enough.");

	
	
	$.validator.addMethod("noMoreThan2Duplicates", function(value) {

		for (var i = 0; i < value.length - 2; i++) {
			if (value.charAt(i) == value.charAt(i + 1) && value.charAt(i) == value.charAt(i + 2)) {
				return false;
			}
		}
		return true;
	}, "Password must not contain more than two duplicate characters in a row");
	
	
	
	
	$.validator.addMethod('passwordMatching', function(value, element) {

		if ($("#registerPassword").val() != $("#confirmPassword").val()) {
			return false;
		} else {
			return true;
		}

	}, "Your Passwords Must Match");

	
	
	
	$("#registerForm").validate({
		
		debug : true,
		
		submitHandler : function(form) {
			$(form).ajaxSubmit();
		},
		submitHandler : function(form) {
			// do other things for a valid form
			form.submit();
		},
		
		
		
		highlight: function(element, errorClass) {
			$(element).removeClass('valid').addClass('error');
		    $(element).fadeOut(function() {
		      $(element).fadeIn();
		    });
		   
		  },
		  
		unhighlight: function(element) {
			    $(element).removeClass('error').addClass('valid');
		},
		  
		  
		rules : {
			username : {
				required : true
			},
			email : {
				required : true,
				email : true
			},
			password : {
				required : true,
				minlength : 10,
				maxlength : 128,
				complexPassword : true,
				noMoreThan2Duplicates : true
			},

			confirmPassword : {
				required : true,
				passwordMatching : true,
				minlength : 10,
				maxlength : 128,
				complexPassword : true,
				noMoreThan2Duplicates : true
			}
		},

		messages : {
			
			username :{
				
				required : "Username is required"
		
			},
			
			email : {
				
				required : "Email is required",
				email : "Must be in proper email format"
				
			},

			password : {
				required : 'Password is required',
				minlength : 'Min length is 10',
				maxlength : 'Max length is 128',
				complexPassword : 'Must contain a number and a symbol',
				noMoreThan2Duplicates : 'No more than 2 characters in a row'

			},

			confirmPassword : {
				required : 'Confirmed Password is required',
				passwordMatching : 'Your Passwords Must Match',
				minlength : 'Min length is 10',
				maxlength : 'Max length is 128',
				complexPassword : 'Must contain a number and a symbol',
				noMoreThan2Duplicates : 'No more than 2 characters in a row'
					
			}
		},
		errorClass : "error"

	});
	
	$("#registerForm").popover({   
	    selector: "[data-toggle='popover']",
	    container: "#registerForm",
	    html: true
	  });

});

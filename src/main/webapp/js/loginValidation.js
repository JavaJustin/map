$(document).ready(function() {

	
	$("#loginForm").validate({
		
		debug : true,
		
		submitHandler : function(form) {
			$(form).ajaxSubmit();
		},
		submitHandler : function(form) {
			// do other things for a valid form
			form.submit();
		},
		
		highlight: function(element, errorClass) {
		    $(element).fadeOut(function() {
		      $(element).fadeIn();
		    });
		  },
		
		rules : {
			email : {
				required : true,
				email: true
			},
			password : {
				required : true
			}
		},
		messages : {

			email : {
				required : "Email address required",
			},
			password : {
				required : "Password required"
			}

		},
		errorClass : "error"

	});
	
	
	



});

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set value="Register" var="pageTitle" />

<%@ include file="common/header.jspf"%>

<div id="user-register" class="container text-center">
	<h1>
		<b>Create an account</b>
	</h1>
	<div class="social-buttons">
		<div class="col-md-6 social-btn">
			<a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i>
				Facebook</a>
		</div>
		<div class="col-md-6 social-btn">
			<a href="#" class="btn btn-gp"><i class="fa fa-google-plus"></i>
				Google+</a>
		</div>
	</div>
	<p>
		<b>or</b>
	</p>
	<div>
		<c:url var="registerUrl" value="/register" />
		<form:form id="registerForm" action="${registerUrl}" method="POST"
			modelAttribute="input" name="registerForm">
			<div class="form-group">

				<form:input path="username" id="username" class="form-control"
					name="username" type="text" placeholder="Username" />
			</div>
			<div class="form-group">

				<form:input path="email" id="email" class="form-control"
					name="email" type="text" placeholder="Email Address" />
			</div>
			<div class="form-group add-on">
			<form:input path="password" id="registerPassword"
					class="form-control" name="password" type="password"
					placeholder="Password"  />
			<span class="btn fa fa-question-circle" data-toggle="popover" data-placement="right" 
			data-original-title="Password Rules:" data-content="-Must be at least 10 characters long
			-Must contain at least 3 of the following 4 types of characters:">
    </span>
			</div>
			<div class="form-group">
				<form:input path="confirmPassword" id="confirmPassword"
					class="form-control" name="confirmPassword" type="password"
					placeholder="Confirm Password" />
			</div>
			<div id="" class="form-group">
				<button id="registerSubmit" class="btn btn-default" type="submit">Create
					Account</button>
			</div>
		</form:form>
	</div>
</div>

<%@ include file="common/footer.jspf"%>
<%@ include file="common/header.jspf" %>

<div class="container">
	<div class="row text-center" style="margin-top: 232px;">
		<div class="col-md-12">
			<h1 class="text-center" data-aos="fade-right"
				data-aos-duration="3000" style="color: #fafafa;">
				<strong>Welcome to MAP</strong>
			</h1>
			<p class="lead" data-aos="fade-left"
				data-aos-duration="3000">
				<em><b>"The journey of one or a thousand miles begins with a single step." - Lao Tzu</b></em>
			</p>
		</div>
		<div
			class="col-lg-3 col-lg-offset-3 col-md-3 col-md-offset-3 col-sm-3 col-sm-offset-3 col-xs-12"
			data-aos="fade-up" data-aos-duration="3000">
			<div>
				<button class="btn btn-default btn-lg" type="button"><b>Create a game</b></button>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" data-aos="fade-up"
			data-aos-duration="3000">
			<div>
				<button class="btn btn-default btn-lg" type="button"><b>Join a game</b></button>
			</div>
		</div>
	</div>
</div>

<%@ include file="common/footer.jspf" %>
package com.techelevator;

import java.util.Random;

public class SpecialAttack {
	
	
	private Random rn = new Random();
	
	private int max = 10;
	
	private int min = 0;
	
	private int range = max - min + 1;
	
	private int damage = 10;
	
	 private int damageMultiplier = rn.nextInt(range) + min;

	
	public int getDamage() {
		
		return damage + damageMultiplier;
	}

}

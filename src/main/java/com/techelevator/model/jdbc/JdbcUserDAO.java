package com.techelevator.model.jdbc;

import javax.sql.DataSource;

import org.bouncycastle.util.encoders.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.techelevator.User;
import com.techelevator.model.dao.UserDAO;
import com.techelevator.security.PasswordHasher;


@Component
public class JdbcUserDAO implements UserDAO {

	private JdbcTemplate jdbcTemplate;
	private PasswordHasher passwordHasher;

	@Autowired
	public JdbcUserDAO(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
		passwordHasher = new PasswordHasher();
	}

	@Override
	public boolean verifyUser(String email, String password) {

		SqlRowSet result = jdbcTemplate.queryForRowSet("SELECT email, password, salt FROM users WHERE email = ?",
				email);

		if (result.next()) {
			String storedSalt = result.getString("salt");
			String storedPassword = result.getString("password");
			String hashedPassword = passwordHasher.computeHash(password, Base64.decode(storedSalt));
			return storedPassword.equals(hashedPassword);
		} else {

			return false;
		}

	}

	@Transactional
	@Override
	public String registerNewUser(User newUser) {
		
		byte[] salt = passwordHasher.generateRandomSalt();
		String hashedPassword = passwordHasher.computeHash(newUser.getPassword(), salt);
		String saltString = new String(Base64.encode(salt));
        
		
	    jdbcTemplate.update("INSERT INTO users(username, email, password, salt) VALUES (?, ?, ?, ?)",
				newUser.getUsername(), newUser.getEmail(), hashedPassword, saltString);
				
        return "";
		
	}

	@Override
	public User searchForUser(String email) {
		
		User userRetrieved = new User();
		
		SqlRowSet result = jdbcTemplate.queryForRowSet("SELECT * FROM users WHERE email = ?", email);
		if(result.next()){
			userRetrieved = mapRowToUser(result);
		} else {
			userRetrieved = null;
		}

	return userRetrieved;
		
		
		
	}

	private User mapRowToUser(SqlRowSet result) {
		
		User user = new User();
		
		user.setUserId(result.getInt("user_id"));
		user.setEmail(result.getString("email"));
		user.setPassword(result.getString("password"));
		user.setSalt(result.getString("salt"));
		user.setUsername(result.getString("username"));

		return user;
	}
 

}

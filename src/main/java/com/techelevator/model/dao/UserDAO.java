package com.techelevator.model.dao;

import com.techelevator.User;

public interface UserDAO {

	public boolean verifyUser(String email, String password);

	public String registerNewUser(User user);
	
	public User searchForUser(String username);


}

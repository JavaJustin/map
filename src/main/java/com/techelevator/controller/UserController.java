package com.techelevator.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.techelevator.User;
import com.techelevator.model.dao.UserDAO;

@Controller
@SessionAttributes("currentUser")
public class UserController {

	@Autowired
	UserDAO userDAO;

	 /*-----------------------------------------------------------------------------*/
	
	/* REGISTER METHODS */

	@RequestMapping(path = "/register", method = RequestMethod.GET)
	public String inputNewUser(Model modelHolder) {
		
		if(! modelHolder.containsAttribute("input")) {
			modelHolder.addAttribute("input", new User());
		}

		return "register";
	}

	
	@RequestMapping(path="/register", method=RequestMethod.POST)
	public String registerNewUser(@Valid @ModelAttribute("user") User user, 
			RedirectAttributes flash, Model modelHolder) {
	
		modelHolder.addAttribute("currentUser", userDAO.registerNewUser(user));
		flash.addFlashAttribute("user", user);

		return "redirect:/";
	}

	
	 /*-----------------------------------------------------------------------------*/

	
	
	
	
	
	
}

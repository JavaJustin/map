package com.techelevator.controller;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.User;
import com.techelevator.model.dao.UserDAO;

@Controller
@SessionAttributes("currentUser")
public class AuthenticationController {

	private UserDAO userDAO;

	@Autowired
	public AuthenticationController(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String homePage(Model modelHolder) {

		if(! modelHolder.containsAttribute("input")) {
			modelHolder.addAttribute("input", new User());
		}
		
		return "homePage";
	}

	/* LOGIN / LOGOUT METHODS */

	@RequestMapping(path = "/login", method = RequestMethod.POST)
	public String loginUser(Model modelHolder, @ModelAttribute("input") User user) {
				
		boolean verified = userDAO.verifyUser(user.getEmail(), user.getPassword());
		if (verified) {
			User verifiedUser = userDAO.searchForUser(user.getEmail());
			modelHolder.addAttribute("currentUser", verifiedUser);
			return "redirect:/";
		}

		return "redirect:/";
	}
	
	@RequestMapping(path = "/logout", method = RequestMethod.GET)
	public String logout1(ModelMap model, HttpSession session) {
		model.remove("currentUser");
		session.removeAttribute("currentUser");
		return "redirect:/";
	}

	@RequestMapping(path = "/logout", method = RequestMethod.POST)
	public String logout2(ModelMap model, HttpSession session) {
		model.remove("currentUser");
		session.removeAttribute("currentUser");
		return "redirect:/";
	}


	 /*---------------------------------------------------------------------------*/


	
	
	
	
}

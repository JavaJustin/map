package com.techelevator;

public class Game {

	private int gameMasterId;
	
	private int numberOfPlayers;
	
	private int playerOneId;
	
	private int playerTwoId;

	private int playerThreeId;

	private int playerFourId;

	private String mapImage;
	
	private int nodes;
	
	
	
	
	public int getNodes() {
		return nodes;
	}

	public void setNodes(int nodes) {
		this.nodes = nodes;
	}

	public String getMapImage() {
		return mapImage;
	}

	public void setMapImage(String mapImage) {
		this.mapImage = mapImage;
	}

	public int getGameMasterId() {
		return gameMasterId;
	}

	public void setGameMasterId(int gameMasterId) {
		this.gameMasterId = gameMasterId;
	}

	public int getNumberOfPlayers() {
		return numberOfPlayers;
	}

	public void setNumberOfPlayers(int numberOfPlayers) {
		this.numberOfPlayers = numberOfPlayers;
	}

	public int getPlayerOneId() {
		return playerOneId;
	}

	public void setPlayerOneId(int playerOneId) {
		this.playerOneId = playerOneId;
	}

	public int getPlayerTwoId() {
		return playerTwoId;
	}

	public void setPlayerTwoId(int playerTwoId) {
		this.playerTwoId = playerTwoId;
	}

	public int getPlayerThreeId() {
		return playerThreeId;
	}

	public void setPlayerThreeId(int playerThreeId) {
		this.playerThreeId = playerThreeId;
	}

	public int getPlayerFourId() {
		return playerFourId;
	}

	public void setPlayerFourId(int playerFourId) {
		this.playerFourId = playerFourId;
	}


	
	
}
